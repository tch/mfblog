<?php
namespace App\Table;

use App\App;
class Article extends Table{

    protected static $table = "articles";

    public static function find($id){
        return self::query('SELECT articles.id, articles.title, articles.contenu, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        WHERE articles.id = ?', [$id], true);

    }

    public static  function getLast(){
        return self::query('SELECT articles.id, articles.title, articles.contenu, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        ORDER BY articles.date DESC');
    }

    public static function lastByCategory($id){
        return self::query('SELECT articles.id, articles.title, articles.contenu, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        WHERE category_id = ?
        ORDER BY articles.date DESC', [$id]);
    }

    public function getUrl(){
        return 'index.php?p=article&id='.$this->id;
    }

    public function getExtrait(){
        return substr($this->contenu, 0, 100).'...';
    }


}