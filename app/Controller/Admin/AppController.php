<?php
/**
 * Created by PhpStorm.
 * User: GILLES
 * Date: 17/11/2015
 * Time: 11:14
 */

namespace App\controller\Admin;

use \App;
use \Core\Auth\DatabaseAuth;

class AppController extends \App\Controller\AppController
{
  public function __construct(){
      parent::__construct();
      $app = App::getInstance();
      $auth = new DatabaseAuth($app->getDb());
      if(!$auth->logged()){
          $this->forbidden();
      }
  }

}