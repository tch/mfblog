<?php

namespace App\Controller\Admin;

use \Core\HTML\BootstrapForm;

class PostsController extends AppController{

    public function __construct(){
        parent::__construct();
        $this->loadModel('Article');

    }

    public function index(){
        $articles  = $this->Article->all();
        $this->render('admin.posts.index', compact('articles'));
    }

    public function add(){
        if(!empty($_POST)){
            $result = $this->Article->create([
                'title' => $_POST['title'],
                'subtitle' => $_POST['subtitle'],
                'contenu' => $_POST['contenu'],
                'category_id' => $_POST['category_id'],
            ]);
            if($result){
                // header('Location: admin.php?p=posts.edit&id='.App::getInstance()->getDb()->lastInsertId());
                header('Location: index.php?p=admin.posts.index');
            }
        }
        $this->loadModel('Category');
        $categories = $this->Category->extract('id', 'libelle');

        $form = new BootstrapForm($_POST);

        $this->render('admin.posts.add', compact('categories', 'form'));

    }


    public function edit(){
        if(!empty($_POST)){
            $result = $this->Article->update($_GET['id'], [
                'title' => $_POST['title'],
                'subtitle' => $_POST['subtitle'],
                'contenu' => $_POST['contenu'],
                'category_id' => $_POST['category_id'],
            ]);
            if($result){
                return $this->index();
            }
        }

        $post = $this->Article->find($_GET['id']);
        $this->loadModel('Category');
        $categoris = $this->Category->extract('id', 'titre');

        $form = new BootstrapForm($post[0]);

        $this->render('admin.posts.add', compact('categories', 'form'));

    }

    public function delete(){

        if(!empty($_POST)){
           $result =  $this->Article->delete($_POST['id']);
            return $this->index();
        }
    }


}