<?php

namespace App\Controller\Admin;

use \Core\HTML\BootstrapForm;

class CategoriesController extends AppController{

    public function __construct(){
        parent::__construct();
        $this->loadModel('Category');

    }

    public function index(){
        $item  = $this->Category->all();
        $this->render('admin.categories.index', compact('item'));
    }

    public function add(){
        if(!empty($_POST)){
            $result = $this->Category->create([
                'libelle' => $_POST['libelle']
            ]);
            if($result){
                // header('Location: admin.php?p=posts.edit&id='.App::getInstance()->getDb()->lastInsertId());
                header('Location: index.php?p=admin.posts.index');
            }
        }
        $this->render('admin.categories.add', compact('categories', 'form'));

    }


    public function edit(){
        if(!empty($_POST)){
            $result = $this->Article->update($_GET['id'], [
                'libelle' => $_POST['libelle'],

            ]);
            if($result){
                return $this->index();
            }
        }

        $category = $this->Category->find($_GET['id']);

        $form = new BootstrapForm($category[0]);

        $this->render('admin.categories.edit', compact('form'));

    }

    public function delete(){

        if(!empty($_POST)){
           $result =  $this->Category->delete($_POST['id']);
            return $this->index();
        }
    }


}