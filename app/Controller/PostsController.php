<?php

namespace App\Controller;

use Core\Controller\Controller;


class PostsController extends AppController{

    public function __construct(){
        parent::__construct();
        $this->loadModel('Article');
        $this->loadModel('Category');
    }

    public function index(){
        $posts = $this->Article->last();

        $this->render('articles.index', compact('posts'));

    }

    public function categories(){
        $categories = $this->Category->all();
        $this->render('articles.categories', compact('categories'));

    }

    public function category(){
        $category = $this->Category->find($_GET['id']);
        $articles = $this->Article->all();
        //$articles = $this->Article->lastByCategory($_GET['id']);
        //$categories = $this->Category->all();

        if($category === false){
            $this->notFound();
        }
        $this->render('articles.category', compact('category', 'articles'));
    }

    public function show(){
        $article = $this->Article->findWithCategory($_GET['id']);
        $this->render('articles.single', compact('article'));
    }


}