<?php
/**
 * Created by PhpStorm.
 * User: GILLES
 * Date: 17/11/2015
 * Time: 14:16
 */

namespace App\Controller;


use Core\HTML\BootstrapForm;
use Core\Auth\DatabaseAuth;
use \App;

class UsersController extends AppController
{
    public function login(){

        $errors = false;
        if(!empty($_POST)){
            $auth = new DatabaseAuth(App::getInstance()->getDb());
            if($auth->login($_POST['username'], $_POST['password'])){
                header('Location: index.php?p=admin.posts.index');
            }else{
               $errors = true;
            }

        }

        $form = new BootstrapForm($_POST);

        $this->render('users.login', compact('form', 'errors'));
    }

}