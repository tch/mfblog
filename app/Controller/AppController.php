<?php
/**
 * Created by PhpStorm.
 * User: GILLES
 * Date: 17/11/2015
 * Time: 11:14
 */

namespace App\Controller;


use Core\Controller\Controller;
use \App;

class AppController extends Controller
{
    protected $template = 'default';


    public function __construct(){
        $this->viewPath = ROOT.'/app/views/';
    }

    public function loadModel($modelName){
        $this->$modelName = App::getInstance()->getTable($modelName);
    }

}