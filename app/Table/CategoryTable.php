<?php
/**
 * Created by PhpStorm.
 * User: GILLES
 * Date: 08/11/2015
 * Time: 16:57
 */

namespace App\Table;

use Core\Table\Table;

class CategoryTable extends Table
{
    protected $table = "categories";

    /**
     * recupere l'article en liant avec la categorie
     * @return \App\Entity\CategoryEntity
     */
    public function find($id){
        return $this->query("
        SELECT categories.id, categories.libelle
        FROM categories
        WHERE categories.id = ?", [$id], true);

    }

}
