<?php
/**
 * Created by PhpStorm.
 * User: GILLES
 * Date: 08/11/2015
 * Time: 16:56
 */

namespace App\Table;
use Core\Table\Table;


class ArticleTable extends Table
{
    protected $table = "articles";

    /**
     * recupere les derniers articles
     * @return array
     */
    public function last(){
        return $this->query("
        SELECT articles.id, articles.title, articles.subtitle, articles.contenu, articles.date, articles.category_id, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        ORDER BY articles.date DESC");

    }

    /**
     * recupere l'article en liant avec la categorie
     * @return \App\Entity\ArticleEntity
     */
    public function findWithCategory($id){
        return $this->query("
        SELECT articles.id, articles.title, articles.subtitle, articles.contenu, articles.date, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        WHERE articles.id = ?", [$id], true);

    }

    /**
     * recupere les derniers articles de la categorie demand�e
     * @return array
     */
    public function lastByCategory($cat_id){
        return $this->query("
        SELECT articles.id, articles.title, articles.subtitle, articles.contenu, articles.date, categories.libelle as categorie
        FROM articles
        LEFT JOIN categories ON category_id = categories.id
        WHERE articles.category_id = ?
        ORDER BY articles.date DESC", [$cat_id]);

    }

}