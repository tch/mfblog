<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Connexion Administration</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

        <?php if($errors): ?>
            <div class="alert alert-danger">
                Identifiants incorrects
            </div>
        <?php endif; ?>

        <form method="post">
            <div class="text-center">
            <?= $form->input('username', 'Pseudo'); ?>
            <?= $form->input('password', 'Mot de passe', ['type' => 'password']); ?>
            <button class="btn btn-primary">Connexion</button>
            </div>
        </form>

    </div>
</div>
