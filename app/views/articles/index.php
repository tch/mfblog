<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Mini blog framework</h1>
                    <hr class="small">
                    <span class="subheading">Un petit framework pour création/gestion d'un blog</span>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="row">
        <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
            <?php foreach($posts as $post): ?>

            <div class="post-preview">
                <a href="<?= $post->url; ?>">
                    <h2 class="post-title">
                        <?= $post->title; ?>
                    </h2>
                    <h3 class="post-subtitle">
                        <?= $post->extrait; ?>
                    </h3>
                </a>
                <p>Catégorie : <?= $post->categorie; ?></p>
                <p class="post-meta">Posté par <a href="#">Utilisateur</a>, le 24 octobre, 2015</p>
            </div>
            <hr>
            <?php endforeach; ?>

        </div>
    </div>
</div>


<hr>

