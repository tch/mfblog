<header class="intro-header" style="background-image: url('img/post-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>
                        <h1><?= $article[0]->title ?></h1></h1>
                    <h2 class="subheading"><?= $article[0]->subtitle ?></h2>
                    <span class="meta">Posté <a href="#"></a> le 25 août, 2015</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p><?= $article[0]->contenu ?></p>
            </div>
        </div>
    </div>
</article>
