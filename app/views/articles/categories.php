<header class="intro-header" style="background-image: url('img/about-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Categories</h1>
                    <hr class="small">
                    <span class="subheading">Les différentes categories</span>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
        <?php foreach($categories as $categorie): ?>


            <div class="text-center">
                <a href="index.php?p=posts.category&id=<?= $categorie->id; ?>">
                    <h2 class="post-title">
                        <?= $categorie->libelle; ?>
                    </h2>
            </div>
            <hr>
        <?php endforeach; ?>
    </div>
</div>
</div>


<hr>