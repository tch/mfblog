<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Administration</h1>
                    <hr class="small">
                    <span class="subheading">gérer les articles</span>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="row">
    <div class="text-center">
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Titre</td>
            <td>Actions</td>

        </tr>
        </thead>
        <tbody>
        <?php foreach($articles as $post): ?>
        <tr>
            <td><?= $post->id ?></td>
            <td><?= $post->title ?></td>
            <td>
                <a class="btn btn-primary" href="?p=admin.posts.edit&id=<?= $post->id; ?>">Editer</a>

                <form method="post" action="?p=admin.posts.delete" style="display: inline;">
                    <input type="hidden" name="id" value="<?= $post->id; ?>">
                    <button type="submit" class="btn btn-danger" href="?p=admin.posts.delete&id=<?= $post->id; ?>">Supprimer</button>
                </form>
            </td>
        </tr>
        <?php endforeach ?>
        </tbody>

    </table>
        <p>
            <a href="?p=admin.posts.add" class="btn btn-success">Ajouter</a>
        </p>
    </div>

</div>


<hr>
