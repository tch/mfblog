<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Ajouter Catégorie</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<div class="row">
    <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">

        <form method="post">
            <div class="text-center">
                <?= $form->input('libelle', 'Libellé de la catégorie'); ?>
                <button class="btn btn-primary">Ajouter</button>
            </div>
        </form>

    </div>
</div>