
<header class="intro-header" style="background-image: url('img/home-bg.jpg')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>Administraion</h1>
                    <hr class="small">
                    <span class="subheading">gérer les catégories</span>
                </div>
            </div>
        </div>
    </div>
</header>


<div class="row">
    <div class="text-center">
    <table class="table">
        <thead>
        <tr>
            <td>ID</td>
            <td>Libellé</td>
            <td>Actions</td>

        </tr>
        </thead>
        <tbody>
        <?php foreach($item as $category): ?>
        <tr>
            <td><?= $category->id ?></td>
            <td><?= $category->libelle ?></td>
            <td>
                <a class="btn btn-primary" href="?p=admin.categories.edit&id=<?= $category->id; ?>">Editer</a>

                <form method="post" action="?p=admin.categories.delete" style="display: inline;">
                    <input type="hidden" name="id" value="<?= $category->id; ?>">
                    <button type="submit" class="btn btn-danger">Supprimer</button>
                </form>
            </td>
        </tr>
        <?php endforeach ?>
        </tbody>

    </table>
        <p>
            <a href="?p=admin.categories.add" class="btn btn-success">Ajouter</a>
        </p>
    </div>

</div>


<hr>
