
<?php
use Core\Auth\DatabaseAuth;
define('ROOT', dirname(__DIR__));
require ROOT.'/app/App.php';
App::load();

if(isset($_GET['p'])){
    $page = $_GET['p'];
}else{
    $page = 'admin';
}

//Auth
$app = App::getInstance();
$auth = new DatabaseAuth($app->getDb());
if(!$auth->logged()){
    $app->forbidden();
}
ob_start();
if($page === 'admin'){
    require ROOT . '/public/views/admin/posts/index.php';
}elseif($page === 'posts.edit'){
    require ROOT . '/public/views/admin/posts/edit.php';
}elseif($page === 'posts.add'){
    require ROOT . '/public/views/admin/posts/add.php';
}elseif($page === 'posts.delete'){
    require ROOT . '/public/views/admin/posts/delete.php';
} elseif($page === 'categories.edit'){
    require ROOT . '/public/views/admin/categories/edit.php';
}elseif ($page === 'categories.index'){
    require ROOT . '/public/views/admin/categories/index.php';
}elseif($page === 'categories.add'){
    require ROOT . '/public/views/admin/categories/add.php';
}elseif($page === 'categories.delete'){
    require ROOT . '/public/views/admin/categories/delete.php';
}
$header = ob_get_clean();
$content = ob_get_clean();

require ROOT . '/public/views/default.php';


